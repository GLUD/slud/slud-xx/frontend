import { ImageForm } from "./image.model";
import { SocialNetwork } from "./social.model";

export interface SpeakerResponseApi {
  data:SpeakerResponseApiData[];
  meta:SpeakerResponseApiMeta;
}

export interface SpeakerResponseApiData{
  id:number;
  attributes: SpeakerResponseApiDataAttributes;
}
export interface SpeakerResponseApiDataAttributes{
  createdAt:string;
  updatedAt:string;
  publishedAt:string;
  name:string;
  degree: string;
  alias:string;
  contact:string;
  email:string;
  typeOfIdentifier:string;
  identifier:string;
  slug:string;
  profesional_description:string;
  socialNetworks:SpeakerResponseSocialNetwortks[];
  image:SpeakerResponseApiDataAttributesImage;
}

export interface SpeakerResponseSocialNetwortks {
  id: number;
  name: string;
  url: string;
}

export interface SpeakerResponseApiDataAttributesSocialnetworks{
  data:SpeakerResponseApiDataAttributesSocialnetworksData[];
}
export interface SpeakerResponseApiDataAttributesSocialnetworksData{
  id:number;
  attributes:SpeakerResponseApiDataAttributesSocialnetworksDataAttributes;
}
export interface SpeakerResponseApiDataAttributesSocialnetworksDataAttributes{
  name:string;
  url:string;
  createdAt:string;
  updatedAt:string;
  publishedAt:string;
}
export interface SpeakerResponseApiDataAttributesImage{
  data:SpeakerResponseApiDataAttributesImageData;
}
export interface SpeakerResponseApiDataAttributesImageData{
  id:number;
  attributes: SpeakerResponseApiDataAttributesImageDataAttributes;
}
export interface SpeakerResponseApiDataAttributesImageDataAttributes{
  name:string;
  alternativeText:string;
  caption:string;
  width:number;
  height:number;
  formats:SpeakerResponseApiDataAttributesImageDataAttributesFormats;
  hash:string;
  ext:string;
  mime:string;
  size:number;
  url:number;
  previewUrl:string;
  provider:string;
  provider_metadata:string;
  createdAt:string;
  updatedAt:string;
}

export interface SpeakerResponseApiDataAttributesImageDataAttributesFormats{
  small:SpeakerResponseApiDataAttributesImageDataAttributesFormartsType;
  medium:SpeakerResponseApiDataAttributesImageDataAttributesFormartsType;
  thumbnail:SpeakerResponseApiDataAttributesImageDataAttributesFormartsType;
}
export interface SpeakerResponseApiDataAttributesImageDataAttributesFormartsType{
  ext:string;
  url:string;
  hash:string;
  mime:string;
  name:string;
  path:string;
  size:number;
  width:number;
  height:number;
}
export interface SpeakerResponseApiMeta{
  pagination:SpeakerResponseApiMetaPagination;
}
export interface SpeakerResponseApiMetaPagination{
  page:number;
  pageSize:number;
  pageCount:number;
  total:number;
}

export interface SpeakerForm  {
  full_name: string;
  alias: string;
  contact: string;
  email: string;
  typeOfIdentifier?: string;
  identifier?: string;
  socialNetworks: SocialNetwork[];
  image?: ImageForm;
}

export interface Speaker {
  id?: number;
  full_name: string;
  alias: string;
  contact: string;
  email: string;
  typeOfIdentifier?: string;
  identifier?: string;
  socialNetworks?: SocialNetwork[];
  image?: ImageForm;
  url?: string;
}
