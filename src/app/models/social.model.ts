export type SocialNetwork = {
    name: string
    url: string
}