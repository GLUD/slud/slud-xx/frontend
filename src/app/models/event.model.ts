export type Event = {
    id: number;
    year: number;
    name: string;
    image: string | null;
}
