type Pagination = {
    page: number
    pageSize: number
    pageCount: number
    total: number
}

type Meta = {
    pagination: Pagination
}

export type ApiResponse = {
    data: []
    meta: Meta
}

export type ApiResponseSpeakers = {
    data: Daum[]
    meta: Meta
}

export interface Daum {
    id: number
    attributes: Attributes
}

export interface Attributes {
    createdAt: string
    updatedAt: string
    publishedAt: string
    full_name: string
    alias: string
    contact: string
    email: string
    typeOfIdentifier: string
    identifier: string
    slug?: (string | null)
    image: Image
}

export interface Image {
    data?: (Data | null)
}

export interface Data {
    id: number
    attributes: Attributes2
}

export interface Attributes2 {
    name: string
    alternativeText: any
    caption: any
    width: number
    height: number
    formats: Formats
    hash: string
    ext: string
    mime: string
    size: number
    url: string
    previewUrl: any
    provider: string
    provider_metadata: any
    createdAt: string
    updatedAt: string
}

export interface Formats {
    thumbnail: Thumbnail
}

export interface Thumbnail {
    ext: string
    url: string
    hash: string
    mime: string
    name: string
    path: any
    size: number
    width: number
    height: number
}

