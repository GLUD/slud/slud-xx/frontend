export interface Conference{
    name: string;
    description: string;
    date: Date;
    link: string;
    speaker:SpeakerConference[];
}

export interface SpeakerConference {
  name: string;
  degree: string;
  image: string;
};
