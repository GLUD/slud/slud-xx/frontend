import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventCarouselComponent } from './components/event-carousel/event-carousel.component';
import { SwiperModule } from 'swiper/angular';
import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';


// Material design


//Components
import { ScheduleComponent } from './components/schedule/schedule.component';
import { SponsorsComponent } from './components/sponsors/sponsors.component';
import { FooterComponent } from './components/footer/footer.component';
import { SpeakersComponent } from './components/speakers/speakers.component';
import { CronogramComponent } from './components/cronogram/cronogram.component';

import { StarfieldComponent } from './components/starfield/starfield.component';
import { CountDownComponent } from './components/count-down/count-down.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { GradientButtonComponent } from './components/gradient-button/gradient-button.component';
import { SponsorCardComponent } from './components/sponsors/sponsorCard/sponsor-card/sponsor-card.component';

import { HttpClientModule } from '@angular/common/http';
import { LandingComponent } from './pages/landing/landing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

// Material Modules
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatListModule } from "@angular/material/list";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select'
import { MatExpansionModule } from '@angular/material/expansion';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const mat_modules = [
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatToolbarModule,
  MatDialogModule,
  MatSelectModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatTableModule,
  MatProgressSpinnerModule,
];



@NgModule({
  declarations: [
    AppComponent,
    ScheduleComponent,
    SpeakersComponent,
    EventCarouselComponent,
    FooterComponent,
    CronogramComponent,
    LandingComponent,
    DialogConfirmComponent,
    StarfieldComponent,
    GradientButtonComponent,
    CountDownComponent,
    NavbarComponent,
    SponsorsComponent,
    SponsorCardComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    HttpClientModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    ...mat_modules
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: environment.recaptcha.siteKey,
      } as RecaptchaSettings,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
