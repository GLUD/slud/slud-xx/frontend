import { Component, ElementRef, ViewChild, Input } from '@angular/core';

type RemainingDate<T> = {
  days: T
  hours: T
  minutes: T
  seconds: T
}

@Component({
  selector: 'app-count-down',
  templateUrl: './count-down.component.html',
  styleUrls: ['./count-down.component.css']
})
export class CountDownComponent {
  @Input() targetDate: Date = new Date(2023, 9, 23, 8, 0, 0);
  @ViewChild('days', { static: true }) days: ElementRef;
  @ViewChild('hours', { static: true }) hours: ElementRef;
  @ViewChild('minutes', { static: true }) minutes: ElementRef;
  @ViewChild('seconds', { static: true }) seconds: ElementRef;

  remainingDate: RemainingDate<number> = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  };;
  countdownEnded: boolean = false;
  

  constructor() {
    const { seconds, minutes, hours, days } = this.remainingDate;
    this.countdownEnded = days === 0 && hours === 0 && minutes === 0 && seconds === 0;
  }

  ngAfterViewInit() {
    const timer = setInterval(() => {
      this.remainingDate = this.getRemainingTime(this.targetDate);
      this.setIntoTemplate(this.remainingDate);
      const { seconds, minutes, hours, days } = this.remainingDate;
      if (days === 0 && hours === 0 && minutes === 0 && seconds === 0) {
        clearInterval(timer);
      }
    }, 1000);
  }

  localeDate = (date?: Date) => {
    return date || new Date();
  }

  mapValues = (object: { [key: string]: unknown }, iterator: (key: unknown) => void) => {
    return Object.keys(object).reduce((acc, key) => {
      acc[key] = iterator(object[key])
      return acc
    }, {})
  }

  fillZeros = (remainingDate: RemainingDate<number>) => {
    return this.mapValues(remainingDate, (value) => `${value}`.padStart(2, '0')) as RemainingDate<string>
  }

  alwaysPositive = (value: number) => Math.max(0, value)

  getRemainingTime = (targetDate: Date) => {
    const currentDate = this.localeDate()
    const diff = targetDate.getTime() - currentDate.getTime()
    const days = this.alwaysPositive(Math.floor(diff / (1000 * 60 * 60 * 24)))
    const hours = this.alwaysPositive(Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)))

    const minutes = this.alwaysPositive(Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60)))

    const seconds = this.alwaysPositive(Math.floor((diff % (1000 * 60)) / 1000))
    return { days, hours, minutes, seconds }
  }

  setIntoTemplate(remainingDate: RemainingDate<number>){
    const { days, hours, minutes, seconds } = remainingDate;
    this.days.nativeElement.innerText = days;
    this.hours.nativeElement.innerText = hours;
    this.minutes.nativeElement.innerText = minutes;
    this.seconds.nativeElement.innerText = seconds;
  }


}
