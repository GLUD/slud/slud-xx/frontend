import { Component, ElementRef, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';

const COUNT = 500;
const SPEED = 0.05;

class Star {
  x: number;
  y: number;
  z: number;
  xPrev: number;
  yPrev: number;

  constructor(x = 0, y = 0, z = 0) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.xPrev = x;
    this.yPrev = y;
  }

  update(width: number, height: number, speed: number) {
    this.xPrev = this.x;
    this.yPrev = this.y;
    this.z += speed * 0.0675;
    this.x += this.x * (speed * 0.0225) * this.z;
    this.y += this.y * (speed * 0.0225) * this.z;
    if (
      this.x > width / 2 ||
      this.x < -width / 2 ||
      this.y > height / 2 ||
      this.y < -height / 2
    ) {
      this.x = Math.random() * width - width / 2;
      this.y = Math.random() * height - height / 2;
      this.xPrev = this.x;
      this.yPrev = this.y;
      this.z = 0;
    }
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.lineWidth = this.z;
    ctx.beginPath();
    ctx.moveTo(this.x, this.y);
    ctx.lineTo(this.xPrev, this.yPrev);
    ctx.stroke();
  }
}

@Component({
  selector: 'app-starfield',
  template: `<canvas #starfieldCanvas></canvas>`,
  styles: [
    `
      canvas {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
      }
    `,
  ],
})
export class StarfieldComponent implements AfterViewInit, OnDestroy {
  @ViewChild('starfieldCanvas', { static: false }) canvasRef: ElementRef;
  
  private stars = Array.from({ length: COUNT }, () => new Star(0, 0, 0));
  private rafId = 0;
  private ctx: CanvasRenderingContext2D;
  private resizeObserver: ResizeObserver;

  ngAfterViewInit(): void {
    this.ctx = this.canvasRef.nativeElement.getContext('2d');

    this.resizeObserver = new ResizeObserver(() => this.setup());
    this.resizeObserver.observe(this.canvasRef.nativeElement);

    this.setup();
  }

  private setup(): void {
    cancelAnimationFrame(this.rafId);

    const canvas = this.canvasRef.nativeElement;
    const { clientWidth: width, clientHeight: height } = canvas;
    const dpr = window.devicePixelRatio || 1;

    canvas.width = width * dpr;
    canvas.height = height * dpr;
    canvas.style.width = `${width}px`;
    canvas.style.height = `${height}px`;

    this.ctx.clearRect(0, 0, canvas.width, canvas.height);
    this.ctx.resetTransform();
    this.ctx.scale(dpr, dpr);
    this.ctx.translate(width / 2, height / 2);
    this.ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
    this.ctx.strokeStyle = 'white';

    for (const star of this.stars) {
      star.x = Math.random() * width - width / 2;
      star.y = Math.random() * height - height / 2;
      star.z = 0;
    }

    this.rafId = requestAnimationFrame(() => this.frame());
  }

  private frame(): void {
    const canvas = this.canvasRef.nativeElement;
    const { clientWidth: width, clientHeight: height } = canvas;

    this.ctx.fillRect(-width / 2, -height / 2, width, height);
    for (const star of this.stars) {
      star.update(width, height, SPEED);
      star.draw(this.ctx);
    }

    this.rafId = requestAnimationFrame(() => this.frame());
  }

  ngOnDestroy(): void {
    cancelAnimationFrame(this.rafId);
    this.resizeObserver.disconnect();
  }
}
