import { Component, ElementRef, Renderer2, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  @Input() irAlApartado: (path: string) => void;
  @ViewChild('menu', { static: true }) menu: ElementRef;


  constructor(
    private router: Router,
    private renderer: Renderer2
  ) { }

  navigateTo(path: string) {
    this.router.navigate([path]);
  }

  onMenuClick() {
    if (this.menu.nativeElement.classList.contains('hidden')) {
      this.renderer.removeClass(this.menu.nativeElement, 'hidden');
    } else {
      this.renderer.addClass(this.menu.nativeElement, 'hidden');
    }
  }

}
