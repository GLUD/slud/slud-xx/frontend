import { Component,OnInit } from '@angular/core';
import { Conference,SpeakerConference } from 'src/app/models/conference.model';
import { RestService } from 'src/app/services/rest.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-cronogram',
  templateUrl: './cronogram.component.html',
  styleUrls: ['./cronogram.component.css'],
})

export class CronogramComponent implements OnInit {

  constructor(private conferenceSevice:RestService){}

  ngOnInit(): void {
    this.getConferences();
  }

  conferences: Conference[] = [];

  public getConferences(){
    this.conferenceSevice.getConferences(environment.API_URL+"/conferences?populate=speakers.image").subscribe(data => {
      console.log(data[0].date);
      this.conferences = data;
    });
  }

  public getNamesSpeakers(speakers:SpeakerConference[]): string{
    let response = speakers.reduce((accumulator, { name }) => accumulator + name + ',', '');
    response = response.substring(0,response.length-1);
    return response;
  }

}
