import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/services/rest.service';
import { SpeakerResponseApiData } from 'src/app/models/speaker.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.css']
})
export class SpeakersComponent implements OnInit {

  public speakers: SpeakerResponseApiData[] = [];
  constructor(private restService:RestService){}

  ngOnInit(): void {
    this.loadSpeakers();
  }

  public loadSpeakers(): void {
    this.restService.getSpeakers(environment.API_URL+"/speakers?populate=image,socialNetworks").subscribe(response =>{
      this.speakers = response;
    });
  }

  openSeakerForm(): void {
    window.open(environment.SPEAKER_URL, "_blank");
  }
}
