import { Component, ViewEncapsulation } from '@angular/core';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, SwiperOptions, Swiper } from 'swiper';
import { Event } from '../../models/event.model'
import { PastEventsService } from '../../services/past-events.service'

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);


@Component({
  selector: 'app-event-carousel',
  templateUrl: './event-carousel.component.html',
  styleUrls: ['./event-carousel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EventCarouselComponent {
  events: Event[] = [];

  constructor(private pastEventsService: PastEventsService) { }
  
  config: SwiperOptions = {
    loop: false,
    effect: 'cards',
    centeredSlidesBounds: true,
    slidesPerView: 4,
    spaceBetween: 50,
    navigation: true,
    pagination: { clickable: true },
    scrollbar: { draggable: true },
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      400: {
        slidesPerView: 1
      },
      800: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      },
      1400: {
        slidesPerView: 4
      }
    }
  };

  ngOnInit(): void {
    this.loadEvents();
  }

  loadEvents(): void {
    this.pastEventsService.getData().subscribe( data => {
      this.events=data;
      this.events.sort((a,b) => {
        return a.year - b.year;
      })
    }, error => console.log(error));
  }

  
}
