import { Component, Input  } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'gradient-button',
  templateUrl: './gradient-button.component.html',
  styleUrls: ['./gradient-button.component.css']
})
export class GradientButtonComponent {

  @Input() textOne: string = '';
  @Input() textTwo: string = '';
  @Input() route: string = '';

  constructor(private router: Router) { }

  navigateTo(){
    window.open(environment.EXTERNALS_URL, "_blank");
  }
}
