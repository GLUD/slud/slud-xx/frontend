import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Sponsor } from 'src/app/models/sponsor';
@Component({
  selector: 'app-sponsor-card',
  template: `
    <div class="card">
      <img src={{sponsor.url}}/>
    </div>
  `,
  styleUrls: ['./sponsor-card.component.css']
})
export class SponsorCardComponent {
  @Input() sponsor!: Sponsor;
}
