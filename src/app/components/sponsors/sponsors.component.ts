import { Component, inject, OnInit } from '@angular/core';
import { Sponsor } from 'src/app/models/sponsor';
import { SponsorServiceService } from 'src/app/services/sponsor-service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.css']
})
export class SponsorsComponent implements OnInit {
  sponsors: Sponsor[] = [];
  sponsorSevice: SponsorServiceService = inject(SponsorServiceService);
  counter: number = 0;

  ngOnInit(): void {
    this.sponsorSevice.getAllSponsors().subscribe((response) =>  {
      this.sponsors = response;
    });
  }

  moreCounter(): number {
    if(this.counter === this.sponsors.length-1){
      return this.counter = 0
    }
    return this.counter++
  }
  lessCounter(){
    if(this.counter == 0){
      return this.counter = this.sponsors.length-1
    }
    return this.counter--
  }

  openSponsorForm(): void {
    window.open(environment.SPONSOR_URL, "_blank");
  }
}
