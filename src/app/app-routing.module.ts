import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './pages/landing/landing.component';
import { CronogramComponent }  from 'src/app/components/cronogram/cronogram.component';

// https://angular.io/api/router/Route
const routes: Routes = [
  {
    title: "INICIO | SLUD XX",
    path: '',
    component: LandingComponent,
  },
  {
    title: "CRONOGRAMA | SLUD XX",
    path: 'cronogram',
    component: CronogramComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    initialNavigation: 'enabledBlocking'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
