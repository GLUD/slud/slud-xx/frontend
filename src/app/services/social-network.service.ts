import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SocialNetworkService {

  API_URL = environment.API_URL

  constructor(private http: HttpClient) { }

  createSocialNetwork(networkData: any): Observable<any> {
    return this.http.post(`${this.API_URL}/social-networks`, networkData)
      .pipe(
        catchError( (error) => {
          console.error("Hubo un error al cargar la networkSocual", error);
          throw new Error(error)
        })
    );
  }

  deleteSocialNetwork(id: any): Observable<any> {
    return this.http.delete<any>(`${this.API_URL}/social-networks/${id}`).pipe(
      map(response => {
        return response;
      })
    );
  }

}
