import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ConferenceService {

  API_URL = environment.API_URL

  constructor(private http: HttpClient) { }

  createConference(conferenceData: any): Observable<any> {
    const headers = new HttpHeaders().set('recaptcha-token-response', conferenceData.data.recaptcha);
    return this.http.post(`${this.API_URL}/conferences`, conferenceData, { headers: headers }).pipe(
      catchError(error => {
        console.error("CREATE:::Hubo un error al cargar la conferencia:", error);
        throw new Error(error);
      })
    );
  }
}
