import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Speaker } from '../models/speaker.model';
import { ApiResponseSpeakers } from '../models/api.model';
import { Observable, catchError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SpeakerService {

  API_URL = environment.API_URL // Asegúrate de reemplazar con la URL correcta

  constructor(private http: HttpClient) { }

  private fromApiResponseToSpeakers(response: ApiResponseSpeakers): Speaker[] {
    const speakers: Speaker[] = [];
    const speakersReponse = response.data;
    speakersReponse.forEach((speaker) => {
      try {
        if (speaker && speaker.attributes) {
          let url: string = '';
          const { attributes, id } = speaker;
          const {
            full_name,
            alias,
            contact,
            email,
            typeOfIdentifier,
            identifier,
            image,
          } = attributes;
          if (image.data !== null) {
            url = `https://strapi.glud.org${image.data.attributes.url}`;
          } else {
            url = `https://strapi.glud.org/uploads/sin_imagen_26e61845b0.png`
          }
          speakers.push(
            {
              id,
              full_name,
              alias,
              contact,
              email,
              typeOfIdentifier,
              identifier,
              url
            }
          )



        }
      } catch (error) {
        console.error(error.message)
      }



    })
    return speakers;
  }

  public getAllSpeakers(): Observable<Speaker[]> {
    return this.http.get<ApiResponseSpeakers>(`${this.API_URL}/speakers?populate=image`).pipe(
      map(response => {
        return this.fromApiResponseToSpeakers(response);
      })
    )
  }

  createSpeaker(speakerData: any): Observable<any> {
    const headers = new HttpHeaders().set('recaptcha-token-response', speakerData.data.recaptcha)
    return this.http.post(`${this.API_URL}/speakers`, speakerData, { headers: headers})
    .pipe(
      catchError( error => {
        console.error("CREATE:::Hubo un error al crear el speker"+ error)
        throw new Error(error)
      })
    );
  }

  updateSpeaker(speakerData: any): Observable<any> {
    const headers =  new HttpHeaders().set('recaptcha-token-response', speakerData.data.recaptcha)
    return this.http.put(`${this.API_URL}/speakers/${speakerData.data.id}`, speakerData, { headers: headers})
    .pipe(
      catchError( error => {
        console.error(error)
        console.error("UPDATE:::Hubo un error al actualizar el speker"+ error)
        throw new Error(error)
      })
    );
  }

}
