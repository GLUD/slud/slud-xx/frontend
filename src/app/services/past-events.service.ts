import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { Event} from '../models/event.model'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PastEventsService {

  private API_URL = environment.API_URL+'/past-events?populate=image';

  constructor(private http: HttpClient) { }

  private fromResponseToEvents(response: any): Event[]  {
    const data: Event[] = [];
    const events = response.data;
    events.map((event) => {
      if (event){
        const { id, attributes } = event;
        const { name, year, image } = attributes;
        const url = `https://strapi.glud.org${image.data.attributes.url}`;
        data.push({ id, name, year, image: url})
      }
    })
    return data;
  }

  public getData(): Observable<any> {
    return this.http.get<any>(this.API_URL).pipe(
      map(response => {
        return this.fromResponseToEvents(response);
      })
    )
  }
}
