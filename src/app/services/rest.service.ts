
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from '../../environments/environment'
import { Conference,SpeakerConference } from 'src/app/models/conference.model';
import { SpeakerResponseApi, SpeakerResponseApiData } from '../models/speaker.model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  API_URL = environment.API_URL

  constructor(private http: HttpClient) { }

  private fromResponseToConferences(response: any): Conference[]  {
    const data: Conference[] = [];
    const conferences = response.data;
    conferences.map((conference) => {
      const speakersData: SpeakerConference[] = [];
      if (conference){
        const { name,description,date,link,speakers } = conference.attributes;
        speakers.data.forEach(({ attributes }) => {
          const { name,degree,image } = attributes;
          const { url } = image.data.attributes;
          speakersData.push({name,degree,image:url});
        });
        data.push({ name,description,date:new Date(date),link,speaker:speakersData });
      }
    })
    return data;
  }

  public getConferences(url:string):Observable<Conference[]>{
    return this.http.get(url).pipe(map((response:any) =>  this.fromResponseToConferences(response)));
  }

  public getSpeakers(url:string):Observable<SpeakerResponseApiData[]>{
    return this.http.get<SpeakerResponseApi>(url).pipe(
      map(response=> response.data)
    );
  }


}
