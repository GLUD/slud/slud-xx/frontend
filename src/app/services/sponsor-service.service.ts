import { Injectable } from '@angular/core';
import { Sponsor } from '../models/sponsor';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
import { Event} from '../models/event.model'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SponsorServiceService {
  sponsors: Sponsor[] = [
    {name: 'Sisas', topic: 'Mentas', url: 'https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6b4e41a670c097c8fd2834579f5d5958&auto=format&fit=crop&w=633&q=80'},
    {name: 'Nocas', topic: 'Perros', url: 'https://images.unsplash.com/photo-1511485977113-f34c92461ad9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=49899e285952107fdfd9415b8d3bf74a&auto=format&fit=crop&w=634&q=80'},
    {name: 'Tales', topic: 'Gatos', url: 'https://images.unsplash.com/photo-1502980426475-b83966705988?ixlib=rb-0.3.5&s=739aef35459daa8aaeaa55363d492bc1&auto=format&fit=crop&w=673&q=80'},
  ]

  constructor(private httpClient: HttpClient){}

  fromResponseToSponsors(response: any): Sponsor[] {
    console.log(response);
    const data: Sponsor[] = [];
    const sponsors = response.data;
    sponsors.map((sponsor: any) => {
      if (sponsor){
        const url: string = `https://strapi.glud.org${sponsor.attributes.image.data.attributes.url}`;
        const name: string = sponsor.attributes.name;
        const topic: string = sponsor.attributes.topic;
        data.push({ name, topic, url});
      }
    })
    return data;
  }

  getAllSponsors(): Observable<Sponsor[]>{
    return this.httpClient.get(environment.API_URL+"/sponsors?populate=image").pipe(
      map(response => {
        return this.fromResponseToSponsors(response);
      })
    )//this.sponsors;
  }
}
