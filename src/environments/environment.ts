export const environment = {
    production: false,
    API_URL: 'https://strapi.glud.org/api',
    EXTERNALS_URL: 'https://forms.office.com/pages/responsepage.aspx?id=74gT1bBqY0OflNVmRKRZcMTZMcbTkHJPlXUQjF_tjsFUNUQ0QUtCTVNVUllCV0dUUkQ3MVg3UUVDRC4u',
    SPONSOR_URL: 'https://forms.office.com/pages/responsepage.aspx?id=74gT1bBqY0OflNVmRKRZcMTZMcbTkHJPlXUQjF_tjsFUM05DVUQwTVFYQ09URlZOS0dPNkNMTFVWQy4u',
    SPEAKER_URL: 'https://forms.office.com/pages/responsepage.aspx?id=74gT1bBqY0OflNVmRKRZcMTZMcbTkHJPlXUQjF_tjsFUNDdZMEk4Vk8wSVpWQ0pXVVpGQVRLWDFOWC4u',
    recaptcha: {
        siteKey: '6LcLfBooAAAAAPEzcBoOmERqxk2MQIhYuMsnN9gD',
    },
};
