/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
  "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors:{
        own_cyan:'#1BAFBF',
      },
      animation:{
        tilt: 'tilt 10s infinite linear'
      },
      keyframes: {
        tilt: {
          "0%, 50%, 100%" : {
            transform: "rotate(0deg)",
          },
          "25%": {
            transform: "rotate(1deg)",
          },
          "75%": {
            transform: "rotate(-1deg)",
          }
        }
      }
    },
  },
  plugins: [],
}
