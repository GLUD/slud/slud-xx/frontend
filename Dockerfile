FROM node:lts-alpine AS builder

WORKDIR /app

RUN apk update
RUN apk upgrade

COPY package.json .

RUN yarn install

COPY . .

RUN yarn build

FROM nginx:stable-alpine-slim AS deploy

COPY --from=builder /app/dist/slud-FrontEnd/browser/ /usr/share/nginx/html/
